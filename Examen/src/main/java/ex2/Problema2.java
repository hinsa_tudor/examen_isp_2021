package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Problema2 extends JFrame {
    JTextField jTextField1;
    JTextField jTextField2;
    JTextField jTextField3;
    JButton button;

    Problema2() {
        setTitle("Calculator suma");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        init();
        setSize(300, 300);
        setVisible(true);
    }

    public static void main(String[] args) {
        new  Problema2();
    }

    public void init() {
        setLayout(null);
        jTextField1 = new JTextField();
        jTextField1.setBounds(10, 20, 200, 20);
        jTextField2 = new JTextField();
        jTextField2.setBounds(10, 70, 200, 20);
        jTextField3 = new JTextField();
        jTextField3.setBounds(10, 120, 200, 20);
        jTextField3.setEditable(false);
        button = new JButton("Adunare");
        button.setBounds(10, 200, 200, 20);
        button.addActionListener(new ButtonSum());
        add(jTextField1);
        add(jTextField2);
        add(jTextField3);
        add(button);
    }

    class ButtonSum implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int t1, t2, s;
            if (button == e.getSource()) {
                t1 = Integer.parseInt(jTextField1.getText());
                t2 = Integer.parseInt(jTextField2.getText());
                s = t1 + t2;
                jTextField3.setText(String.valueOf(s));
            }
        }
    }
}
